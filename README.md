## Prerequisites:
1. install kafka, ballerina 1.2.14, minikube, kubectl, helm, docker, and mysql locally
2. add listener and advertised listener addresses for kafka set to the host.minikube.internal ip address (found by sshing into minikube and checking the value of host.minikube.internal)
3. ensure the option that allows kafka to automatically create new topics is enabled
3. create a mysql user (username: dev, password: Dev@220038) that can access the server from anywhere. Ensure mysql can accept connections from the host.minikube.internal ip address
4. execute the database_setup.sql file in mysql

## Starting the services and client
1. cd into the project's root directory and execute the deploy.sh script
2. cd into the client folder and execute "bal run main"
