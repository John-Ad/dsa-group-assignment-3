import ballerina/io;
import ballerina/kafka;
import ballerina/lang.'string;


//-----------------------------------------------------------
//                DB INIT
//-----------------------------------------------------------
DB_QryHandler dbConn = new ();

//-----------------------------------------------------------
//                PRODUCER SETUP
//-----------------------------------------------------------
kafka:ProducerConfiguration producerConfigs = {
    bootstrapServers: "192.168.49.1:9092",
    clientId: "course_service",
    acks: "all",
    retryCount: 3
};

kafka:Producer producer = new (producerConfigs);
//-----------------------------------------------------------
//
//
//
//
//-----------------------------------------------------------
//                CONSUMER CONFIG
//-----------------------------------------------------------
string[] topics = [
        "course_service"
    ];

kafka:ConsumerConfiguration consumerConfig = {
    bootstrapServers: "192.168.49.1:9092",
    groupId: "course_service",
    topics: topics,
    pollingIntervalInMillis: 1000
};
//-----------------------------------------------------------
//
//
//
//
//-----------------------------------------------------------
//                CONSUMER SERVICE
//-----------------------------------------------------------
listener kafka:Consumer consumer = new (consumerConfig);

service KafkaRequestService on consumer {
    resource function onMessage(any consumerAction, kafka:ConsumerRecord[] records) {
        // Dispatched set of Kafka records to service, We process each one by one.
        foreach var kafkaRecord in records {
            string|() msg = processKafkaRecord(kafkaRecord);
            if msg is string {
                error? _err = processRequest(msg);
            }
        }
    }
}
//-----------------------------------------------------------
//
//
//
//
//-----------------------------------------------------------
//                PROCESS KAFKA RECORDS
//-----------------------------------------------------------
public function processKafkaRecord(kafka:ConsumerRecord kafkaRecord) returns string|() {
    byte[] serializedMsg = <byte[]>kafkaRecord.value;
    string|error msg = 'string:fromBytes(serializedMsg);
    if (msg is string) {
        // Print the retrieved Kafka record.
        // io:println("Topic: ", kafkaRecord.topic, " Received Message: ", msg);
        return msg;
    } else {
        io:println("Error occurred while converting message data", msg);
    }
    return "";
}
//-----------------------------------------------------------
//
//
//
//
//-----------------------------------------------------------
//                PROCESS REQUEST
//-----------------------------------------------------------
function processRequest(string message) returns @tainted error? {

    //----------------------------------
    //     CONVERT STRING TO OBJECT
    //----------------------------------
    any|error request = fromJsonString(message, TYPE_REQ);

    //----------------------------------
    //     CHECK IF ERROR OCCURED
    //----------------------------------
    if request is error {
        io:println(request.detail());
        return request;
    }
    if request is Request {

        //--------------------------------------------
        //     PASS TO REQUEST HANDLER FUNCTIONS
        //--------------------------------------------

        if request.reqType == TYPE_ADD_COURSE_REQ {
            addCourse(request.data);
            return;
        }

        if request.reqType == TYPE_ADD_STUDENT_COURSE_REQ {
            addStudentCourse(request.data);
            return;
        }

        if request.reqType == TYPE_GET_STUDENT_COURSES_REQ {
            getStudentCourses(request.data);
            return;
        }

        if request.reqType == TYPE_GET_ALL_COURSES_REQ {
            // addCourse(request.data);
            return;
        }

        if request.reqType == TYPE_UPDATE_COURSE_REQ {
            updateCourse(request.data);
            return;
        }

    }
}
//-----------------------------------------------------------
//
//
//
//
//-----------------------------------------------------------
//         PRODUCE MESSAGE
//-----------------------------------------------------------
function produceMessage(string topic, any data) {

    //-----------------------------------------
    //         CONVERT DATA TO JSON
    //-----------------------------------------
    json|error dataJson = toJson(data);

    if dataJson is json {

        //-----------------------------------------
        //         CONVERT JSON TO STRING
        //         THEN TO BYTES
        //-----------------------------------------
        string msg = dataJson.toJsonString();
        byte[] msgBytes = msg.toBytes();

        //-----------------------------------------
        //         SEND MESSAGE
        //-----------------------------------------
        var sendResult = producer->send(msgBytes, topic);
        if sendResult is error {
            io:println("failed to send message: ", sendResult);
        } else {
            io:println("successfully sent message");
        }

    } else {
        io:println(dataJson.detail());
    }
}
//-----------------------------------------------------------
//
//
//
//
//
//
//-----------------------------------------------------------
//                ADD COURSE
//-----------------------------------------------------------
function addCourse(string data) {

    io:println("adding course");

    //-------------------------------
    //         CONVERT TO OBJ
    //-------------------------------
    any|error addCourseData = fromJsonString(data, TYPE_ADD_COURSE_REQ);

    //-------------------------------
    //         CHECK FOR ERROR
    //-------------------------------
    if addCourseData is error {
        io:println(addCourseData.detail());
        return;
    }
    if addCourseData is AddCourseRequest {

        //------------------------------------------------------
        //         CREATE RESPONSE OBJ WITH SUCCESS PARAMS
        //------------------------------------------------------
        Response res = {
            status: 0,
            data: "ok"
        };

        //-------------------------------
        //         PERFORM QRY
        //-------------------------------
        ()|error err = dbConn.addCourse(<@untained>addCourseData);

        //--------------------------------------------
        //         SET RES TO ERROR IF ERR OCCURS
        //--------------------------------------------
        if err is error {
            res.status = 1;
            res.data = err.reason();
            io:println(err);
            io:println(err.detail());
        }

        //-------------------------------
        //         SEND RES
        //-------------------------------
        produceMessage(addCourseData.clientTopic, res);
    }
}
//-----------------------------------------------------------
//
//
//
//
//
//
//-----------------------------------------------------------
//                UPDATE COURSE
//-----------------------------------------------------------
function updateCourse(string data) {

    io:println("updating course");

    //-------------------------------
    //         CONVERT TO OBJ
    //-------------------------------
    any|error updateCourseData = fromJsonString(data, TYPE_UPDATE_COURSE_REQ);

    //-------------------------------
    //         CHECK FOR ERROR
    //-------------------------------
    if updateCourseData is error {
        io:println(updateCourseData.detail());
        return;
    }
    if updateCourseData is UpdateCourseRequest {

        //------------------------------------------------------
        //         CREATE RESPONSE OBJ WITH SUCCESS PARAMS
        //------------------------------------------------------
        Response res = {
            status: 0,
            data: "ok"
        };

        //-------------------------------
        //         PERFORM QRY
        //-------------------------------
        ()|error err = dbConn.updateCourse(<@untained>updateCourseData);

        //--------------------------------------------
        //         SET RES TO ERROR IF ERR OCCURS
        //--------------------------------------------
        if err is error {
            res.status = 1;
            res.data = err.reason();
            io:println(err);
            io:println(err.detail());
        }

        //-------------------------------
        //         SEND RES
        //-------------------------------
        produceMessage(updateCourseData.clientTopic, res);
    }
}
//-----------------------------------------------------------
//
//
//
//
//-----------------------------------------------------------
//                ADD STUDENT COURSE
//-----------------------------------------------------------
function addStudentCourse(string data) {

    //-------------------------------
    //         CONVERT TO OBJ
    //-------------------------------
    any|error addStudentCourseData = fromJsonString(data, TYPE_ADD_STUDENT_COURSE_REQ);

    //-------------------------------
    //         CHECK FOR ERROR
    //-------------------------------
    if addStudentCourseData is error {
        io:println(addStudentCourseData.detail());
        return;
    }
    if addStudentCourseData is AddStudentCourseRequest {

        //------------------------------------------------------
        //         CREATE RESPONSE OBJ WITH SUCCESS PARAMS
        //------------------------------------------------------
        Response res = {
            status: 0,
            data: "ok"
        };

        //-------------------------------
        //         PERFORM QRY
        //-------------------------------
        ()|error err = dbConn.addStudentCourse(<@untained>addStudentCourseData);

        //--------------------------------------------
        //         SET RES TO ERROR IF ERR OCCURS
        //--------------------------------------------
        if err is error {
            res.status = 1;
            res.data = err.reason();
            io:println(err);
            io:println(err.detail());
        }

        //-------------------------------
        //         SEND RES
        //-------------------------------
        produceMessage(addStudentCourseData.clientTopic, res);
    }
}
//-----------------------------------------------------------
//
//
//
//
//-----------------------------------------------------------
//                GET STUDENT COURSES
//-----------------------------------------------------------
function getStudentCourses(string data) {

    io:println("getting student courses");

    //-------------------------------
    //         CONVERT TO OBJ
    //-------------------------------
    any|error getStudentCoursesData = fromJsonString(data, TYPE_GET_STUDENT_COURSES_REQ);

    //-------------------------------
    //         CHECK FOR ERROR
    //-------------------------------
    if getStudentCoursesData is error {
        io:println(getStudentCoursesData.detail());
        return;
    }
    if getStudentCoursesData is GetStudentCoursesRequest {

        //------------------------------------------------------
        //         CREATE RESPONSE OBJ WITH SUCCESS PARAMS
        //------------------------------------------------------
        Response res = {
            status: 0,
            data: ""
        };

        //-------------------------------
        //         PERFORM QRY
        //-------------------------------
        Course[]|error err = dbConn.getStudentCourses(<@untained>getStudentCoursesData);

        //--------------------------------------------
        //         SET RES TO ERROR IF ERR OCCURS
        //--------------------------------------------
        if err is error {
            res.status = 1;
            res.data = err.reason();
            io:println(err);
            io:println(err.detail());
        } else {
            //-------------------------------
            //         CONVERT TO JSON
            //-------------------------------
            json|error coursesJson = json.constructFrom(err);
            if coursesJson is error {
                res.status = 1;
                res.data = coursesJson.reason();
            } else {
                res.data = coursesJson.toJsonString();
            }
        }

        io:println("sending response");
        //-------------------------------
        //         SEND RES
        //-------------------------------
        produceMessage(getStudentCoursesData.clientTopic, res);
    }

}
//-----------------------------------------------------------
