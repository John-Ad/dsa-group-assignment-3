import ballerina/io;


//----------------------------
//     REQUEST OBJECT
//----------------------------
public type Request record {
    int reqType;
    string data;
};

//----------------------------
//     RESPONSE OBJECT
//----------------------------
public type Response record {
    int status;
    string data;
};

//----------------------------
//     STORAGE POST RESULT
//----------------------------
public type Storage_RESULT record {
    string RESULT;
};

//-------------------------
//     COURSE OBJECT
//-------------------------
public type Course record {
    string courseName;
    string lectUsername;
    string desc;
};

//-------------------------
//     ADD COURSE REQUEST
//-------------------------
public type AddCourseRequest record {
    string clientTopic;
    string hodUsername;
    string courseName;
    string lectUsername;
    string desc;
};

//-----------------------------
//     UPDATE COURSE REQUEST
//-----------------------------
public type UpdateCourseRequest record {
    string clientTopic;
    string username;
    string courseName;
    string desc;
};

//---------------------------------
//     ADD STUDENT COURSE REQUEST
//---------------------------------
public type AddStudentCourseRequest record {
    string clientTopic;
    string studentUsername;
    string courseName;
};

//---------------------------------
//     GET ALL COURSES REQUEST
//---------------------------------
public type GetAllCoursesRequest record {
    string clientTopic;
};

//---------------------------------
//     GET STUDENT COURSES REQUEST
//---------------------------------
public type GetStudentCoursesRequest record {
    string clientTopic;
    string studentUsername;
};


//---------------------------------------------------
//          CONVERT OBJ TO JSON
//---------------------------------------------------
public function toJson(any data) returns json|error {
    if data is Response {
        return {
            status: data.status,
            data: data.data
        };
    }
    if data is Request {
        return {
            reqType: data.reqType,
            data: data.data
        };
    }
    if data is AddCourseRequest {
        return {
            clientTopic: data.clientTopic,
            hodUsername: data.hodUsername,
            courseName: data.courseName,
            lectUsername: data.lectUsername,
            desc: data.desc
        };
    }
    if data is UpdateCourseRequest {
        return {
            clientTopic: data.clientTopic,
            courseName: data.courseName,
            username: data.username,
            desc: data.desc
        };
    }
    if data is AddStudentCourseRequest {
        return {
            clientTopic: data.clientTopic,
            studentUsername: data.studentUsername,
            courseName: data.courseName
        };
    }
    if data is GetStudentCoursesRequest {
        return {
            clientTopic: data.clientTopic,
            studentUsername: data.studentUsername
        };
    }
    if data is GetAllCoursesRequest {
        return {
            clientTopic: data.clientTopic
        };
    }
    if data is Course {
        return {
            courseName: data.courseName
        };
    }

    return error("public type not recognized");
}

public const TYPE_REQ = 0;
public const TYPE_RES = 1;
public const TYPE_COURSE = 2;
public const TYPE_ADD_COURSE_REQ = 3;
public const TYPE_GET_ALL_COURSES_REQ = 4;
public const TYPE_GET_STUDENT_COURSES_REQ = 5;
public const TYPE_ADD_STUDENT_COURSE_REQ = 6;
public const TYPE_UPDATE_COURSE_REQ = 16;



//---------------------------------------------------
//          CONVERT JSON TO OBJ
//---------------------------------------------------
public function fromJsonString(string jsonStr, int convType) returns @tainted any|error {
    io:StringReader sr = new (jsonStr, "UTF-8");
    json|error jObj = sr.readJson();

    if jObj is error {
        return jObj;
    } else {
        if convType == TYPE_RES {
            return Response.constructFrom(jObj);
        }
        if convType == TYPE_REQ {
            return Request.constructFrom(jObj);
        }
        if convType == TYPE_COURSE {
            return Course.constructFrom(jObj);
        }
        if convType == TYPE_ADD_COURSE_REQ {
            return AddCourseRequest.constructFrom(jObj);
        }
        if convType == TYPE_GET_ALL_COURSES_REQ {
            return GetAllCoursesRequest.constructFrom(jObj);
        }
        if convType == TYPE_GET_STUDENT_COURSES_REQ {
            return GetStudentCoursesRequest.constructFrom(jObj);
        }
        if convType == TYPE_ADD_STUDENT_COURSE_REQ {
            return AddStudentCourseRequest.constructFrom(jObj);
        }
        if convType ==TYPE_UPDATE_COURSE_REQ {
            return UpdateCourseRequest.constructFrom(jObj);
        }
    }
}
