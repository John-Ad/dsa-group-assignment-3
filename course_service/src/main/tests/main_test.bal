import ballerina/io;
import ballerina/test;

# Before Suite Function

@test:BeforeSuite
function beforeSuiteFunc() {
// io:println("I'm the before suite function!");
}

# Before test function

function beforeFunc() {
// io:println("I'm the before function!");
}

# Test function

@test:Config {
}
function testAddCourse() {
    io:println("####################################");
    io:println("######   TESTING ADD COURSE   ######");
    io:println("####################################");

    AddCourseRequest req = {
        clientTopic: "client",
        hodUsername: "john",
        courseName: "dsa",
        lectUsername: "jd",
        desc: "some stuff"
    };

    json|error reqJS = toJson(req);

    if reqJS is json {
        string jsStr = reqJS.toJsonString();
        addCourse(jsStr);
    }
    test:assertTrue(true, msg = "Failed!");
}
@test:Config {
}
function testAddStudentCourse() {
    io:println("############################################");
    io:println("######   TESTING ADD STUDENT COURSE   ######");
    io:println("############################################");

    AddStudentCourseRequest req = {
        clientTopic: "cl",
        studentUsername: "john",
        courseName: "dsa"
    };

    json|error reqJS = toJson(req);

    if reqJS is json {
        string jsStr = reqJS.toJsonString();
        addCourse(jsStr);
    }
    test:assertTrue(true, msg = "Failed!");
}

# After test function

function afterFunc() {
// io:println("I'm the after function!");
}

# After Suite Function

@test:AfterSuite
function afterSuiteFunc() {
// io:println("I'm the after suite function!");
}
