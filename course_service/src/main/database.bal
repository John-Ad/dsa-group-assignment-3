import ballerina/io;
import ballerinax/java.jdbc;

jdbc:Client dbClient = new ({
    url: "jdbc:mysql://192.168.49.1:3306/DSA_3_DB",
    username: "dev",
    password: "Dev@220038",
    dbOptions: {useSSL: false, allowPublicKeyRetrieval: true}
});

type TestRecord record {
    string RESULT;
};

public type DB_QryHandler object {

    public function __init() {
        var ret = dbClient->select("select 'working' as RESULT", TestRecord);
        if ret is table<TestRecord> {
            foreach var row in ret {
                io:println(row.RESULT);
            }
        } else {
            io:println(ret.toString());
        }
    }

    public function addCourse(AddCourseRequest req) returns @tainted ()|error {
        string qryString = "call sp_addCourse('" + req.hodUsername + "', '" + req.courseName + "', '" + req.lectUsername + "', '" + req.desc + "');";
        io:println(qryString);
        var ret = dbClient->select(qryString, Storage_RESULT);
        if ret is table<Storage_RESULT> {
            foreach var row in ret {
                if row.RESULT == "ok" {
                    io:println("course added");
                    return ();
                } else {
                    return error(row.RESULT);
                }
            }
        } else if ret is error {
            return ret;
        }
    }

    public function updateCourse(UpdateCourseRequest req) returns @tainted ()|error {
        string qryString = "call sp_updateCourse('" + req.username + "', '" + req.courseName + "', '" + req.desc + "');";
        io:println(qryString);
        var ret = dbClient->select(qryString, Storage_RESULT);
        if ret is table<Storage_RESULT> {
            foreach var row in ret {
                if row.RESULT == "ok" {
                    io:println("course updated");
                    return ();
                } else {
                    return error(row.RESULT);
                }
            }
        } else if ret is error {
            return ret;
        }
    }

    public function addStudentCourse(AddStudentCourseRequest req) returns @tainted ()|error {
        string qryString = "call sp_addStudentCourse('" + req.studentUsername + "','" + req.courseName + "');";
        io:println(qryString);
        var ret = dbClient->select(qryString, Storage_RESULT);
        if ret is table<Storage_RESULT> {
            foreach var row in ret {
                if row.RESULT == "ok" {
                    io:println("course added");
                    return ();
                } else {
                    return error(row.RESULT);
                }
            }
        } else if ret is error {
            return ret;
        }
    }

    public function getStudentCourses(GetStudentCoursesRequest req) returns @tainted Course[]|error {
        string qryString = "call sp_getStudentCourses('" + req.studentUsername + "');";
        io:println(qryString);
        var ret = dbClient->select(qryString, Course);
        if ret is table<Course> {
            Course[] cArray = [];
            foreach var row in ret {
                cArray.push(row);
            }

            if cArray.length() > 0 {
                return cArray;
            } else {
                return error("no records found");
            }
        } else {
            return <error>ret;
        }
    }
};
