import ballerina/io;

//----------------------------
//     REQUEST OBJECT
//----------------------------
public type Request record {
    int reqType;
    string data;
};

//----------------------------
//     RESPONSE OBJECT
//----------------------------
public type Response record {
    int status;
    string data;
};

//----------------------------
//     STORAGE POST RESULT
//----------------------------
public type Storage_RESULT record {
    string RESULT;
};


const USER_HOD = 1;
const USER_STUDENT = 2;
const USER_LECTURER = 3;

//-------------------------
//     USER OBJECT
//-------------------------
public type User record {
    string username;
    string password;
};

//-------------------------
//     ADD USER REQUEST
//-------------------------
public type AddUserRequest record {
    string clientTopic;
    int userType;
    string username;
    string password;
};

//-------------------------
//     GET USER REQUEST
//-------------------------
public type GetUserRequest record {
    string clientTopic;
    int userType;
    string username;
};


//---------------------------------------------------
//          CONVERT OBJ TO JSON
//---------------------------------------------------
public function toJson(any data) returns json|error {
    if data is Response {
        return {
            status: data.status,
            data: data.data
        };
    }
    if data is Request {
        return {
            reqType: data.reqType,
            data: data.data
        };
    }
    if data is AddUserRequest {
        return {
            clientTopic: data.clientTopic,
            userType: data.userType,
            username: data.username,
            password: data.password
        };
    }
    if data is GetUserRequest {
        return {
            clientTopic: data.clientTopic,
            userType: data.userType,
            username: data.username
        };
    }
    if data is User {
        return {
            username: data.username,
            password: data.password
        };
    }

    return error("public type not recognized");
}

public const TYPE_REQ = 0;
public const TYPE_RES = 1;
public const TYPE_ADD_USER_REQ = 12;
public const TYPE_GET_USER_REQ = 13;
public const TYPE_USER = 14;
public const TYPE_LOGIN_USER_REQ = 15;


//---------------------------------------------------
//          CONVERT JSON TO OBJ
//---------------------------------------------------
public function fromJsonString(string jsonStr, int convType) returns @tainted any|error {
    io:StringReader sr = new (jsonStr, "UTF-8");
    json|error jObj = sr.readJson();

    if jObj is error {
        return jObj;
    } else {
        if convType == TYPE_RES {
            return Response.constructFrom(jObj);
        }
        if convType == TYPE_REQ {
            return Request.constructFrom(jObj);
        }
        if convType == TYPE_ADD_USER_REQ {
            return AddUserRequest.constructFrom(jObj);
        }
        if convType == TYPE_GET_USER_REQ {
            return GetUserRequest.constructFrom(jObj);
        }
        if convType == TYPE_USER {
            return User.constructFrom(jObj);
        }
    }
}


