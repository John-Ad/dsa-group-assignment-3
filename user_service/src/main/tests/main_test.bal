import ballerina/io;
import ballerina/test;

# Before Suite Function

@test:BeforeSuite
function beforeSuiteFunc() {
// io:println("I'm the before suite function!");
}

# Before test function

function beforeFunc() {
// io:println("I'm the before function!");
}

# Test function

@test:Config {
}
function testAddUser() {
    io:println("##################################");
    io:println("######   TESTING ADD USER   ######");
    io:println("##################################");

    AddUserRequest req = {
        clientTopic: "test",
        userType: USER_HOD,
        username: "john",
        password: "pw"
    };

    json|error reqJS = toJson(req);

    if reqJS is json {
        string jsStr = reqJS.toJsonString();
        addUser(jsStr);
    }
}

# After test function

function afterFunc() {
// io:println("I'm the after function!");
}

# After Suite Function

@test:AfterSuite
function afterSuiteFunc() {
// io:println("I'm the after suite function!");
}
