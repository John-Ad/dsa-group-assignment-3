import ballerina/io;
import ballerina/kafka;
import ballerina/lang.'string;


//-----------------------------------------------------------
//                DB INIT
//-----------------------------------------------------------
DB_QryHandler dbConn = new ();


//-----------------------------------------------------------
//                PRODUCER SETUP
//-----------------------------------------------------------
kafka:ProducerConfiguration producerConfigs = {
    bootstrapServers: "192.168.49.1:9092",
    clientId: "user_service",
    acks: "all",
    retryCount: 3
};

kafka:Producer producer = new (producerConfigs);
//-----------------------------------------------------------
//
//
//
//
//-----------------------------------------------------------
//                CONSUMER CONFIG
//-----------------------------------------------------------
string[] topics = [
        "user_service"
    ];

kafka:ConsumerConfiguration consumerConfig = {
    bootstrapServers: "192.168.49.1:9092",
    groupId: "user_service",
    topics: topics,
    pollingIntervalInMillis: 1000
};
//-----------------------------------------------------------
//
//
//
//
//-----------------------------------------------------------
//                CONSUMER SERVICE
//-----------------------------------------------------------
listener kafka:Consumer consumer = new (consumerConfig);

service KafkaRequestService on consumer {
    resource function onMessage(any consumerAction, kafka:ConsumerRecord[] records) {
        // Dispatched set of Kafka records to service, We process each one by one.
        io:println("message received");
        foreach var kafkaRecord in records {
            string|() msg = processKafkaRecord(kafkaRecord);
            if msg is string {
                error? _err = processRequest(msg);
            }
        }
    }
}
//-----------------------------------------------------------
//
//
//
//
//-----------------------------------------------------------
//                PROCESS KAFKA RECORDS
//-----------------------------------------------------------
public function processKafkaRecord(kafka:ConsumerRecord kafkaRecord) returns string|() {
    byte[] serializedMsg = <byte[]>kafkaRecord.value;
    string|error msg = 'string:fromBytes(serializedMsg);
    if (msg is string) {
        // Print the retrieved Kafka record.
        // io:println("Topic: ", kafkaRecord.topic, " Received Message: ", msg);
        return msg;
    } else {
        io:println("Error occurred while converting message data", msg);
    }
    return "";
}
//-----------------------------------------------------------
//
//
//
//
//-----------------------------------------------------------
//                PROCESS REQUEST
//-----------------------------------------------------------
function processRequest(string message) returns @tainted error? {
    io:println("message received: ", message);

    //----------------------------------
    //     CONVERT STRING TO OBJECT
    //----------------------------------
    any|error request = fromJsonString(message, TYPE_REQ);

    //----------------------------------
    //     CHECK IF ERROR OCCURED
    //----------------------------------
    if request is error {
        io:println(request.detail());
        return request;
    }
    if request is Request {

        //--------------------------------------------
        //     PASS TO REQUEST HANDLER FUNCTIONS
        //--------------------------------------------
        if request.reqType == TYPE_ADD_USER_REQ {
            addUser(request.data);
        }
        if request.reqType == TYPE_LOGIN_USER_REQ {
            login(request.data);
        }

    }
}
//-----------------------------------------------------------
//
//
//
//
//-----------------------------------------------------------
//         PRODUCE MESSAGE
//-----------------------------------------------------------
function produceMessage(string topic, any data) {

    //-----------------------------------------
    //         CONVERT DATA TO JSON
    //-----------------------------------------
    json|error dataJson = toJson(data);

    if dataJson is json {

        //-----------------------------------------
        //         CONVERT JSON TO STRING
        //         THEN TO BYTES
        //-----------------------------------------
        string msg = dataJson.toJsonString();
        byte[] msgBytes = msg.toBytes();

        //-----------------------------------------
        //         SEND MESSAGE
        //-----------------------------------------
        var sendResult = producer->send(msgBytes, topic);
        if sendResult is error {
            io:println("failed to send message: ", sendResult);
        } else {
            io:println("successfully sent message");
        }

    } else {
        io:println(dataJson.detail());
    }
}
//-----------------------------------------------------------
//
//
//
//
//
//
//-----------------------------------------------------------
//                ADD USER
//-----------------------------------------------------------
function addUser(string data) {

    io:println("Adding user");

    //-------------------------------
    //         CONVERT TO OBJ
    //-------------------------------
    any|error addUserData = fromJsonString(data, TYPE_ADD_USER_REQ);

    //-------------------------------
    //         CHECK FOR ERROR
    //-------------------------------
    if addUserData is error {
        io:println(addUserData.detail());
        return;
    }
    if addUserData is AddUserRequest {

        //------------------------------------------------------
        //         CREATE RESPONSE OBJ WITH SUCCESS PARAMS
        //------------------------------------------------------
        Response res = {
            status: 0,
            data: "ok"
        };

        //-------------------------------
        //         PERFORM QRY
        //-------------------------------
        ()|error err = dbConn.addUser(<@untained>addUserData);

        //--------------------------------------------
        //         SET RES TO ERROR IF ERR OCCURS
        //--------------------------------------------
        if err is error {
            res.status = 1;
            res.data = err.reason();
            io:println(err);
            io:println(err.detail());
        }

        io:println("sending response");
        //-------------------------------
        //         SEND RES
        //-------------------------------
        produceMessage(addUserData.clientTopic, res);
    }
}
//-----------------------------------------------------------
//
//
//
//
//-----------------------------------------------------------
//                LOGIN
//-----------------------------------------------------------
function login(string data) {

    io:println("logging user in");

    //-------------------------------
    //         CONVERT TO OBJ
    //-------------------------------
    any|error loginUserData = fromJsonString(data, TYPE_ADD_USER_REQ);

    //-------------------------------
    //         CHECK FOR ERROR
    //-------------------------------
    if loginUserData is error {
        io:println(loginUserData.detail());
        return;
    }
    if loginUserData is AddUserRequest {

        //------------------------------------------------------
        //         CREATE RESPONSE OBJ WITH SUCCESS PARAMS
        //------------------------------------------------------
        Response res = {
            status: 0,
            data: "ok"
        };

        //-------------------------------
        //         PERFORM QRY
        //-------------------------------
        ()|error err = dbConn.login(<@untained>loginUserData);

        //--------------------------------------------
        //         SET RES TO ERROR IF ERR OCCURS
        //--------------------------------------------
        if err is error {
            res.status = 1;
            res.data = err.reason();
            io:println(err);
            io:println(err.detail());
        }

        io:println("sending response");
        //-------------------------------
        //         SEND RES
        //-------------------------------
        produceMessage(loginUserData.clientTopic, res);
    }
}
//-----------------------------------------------------------




