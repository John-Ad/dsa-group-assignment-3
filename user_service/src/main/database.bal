import ballerina/io;
import ballerinax/java.jdbc;

jdbc:Client dbClient = new ({
    url: "jdbc:mysql://192.168.49.1:3306/DSA_3_DB",
    username: "dev",
    password: "Dev@220038",
    dbOptions: {useSSL: false, allowPublicKeyRetrieval: true}
});

type TestRecord record {
    string RESULT;
};

public type DB_QryHandler object {

    public function __init() {
        var ret = dbClient->select("select 'working' as RESULT", TestRecord);
        if ret is table<TestRecord> {
            foreach var row in ret {
                io:println(row.RESULT);
            }
        } else {
            io:println(ret.toString());
        }
    }

    public function addUser(AddUserRequest req) returns @tainted ()|error {
        string qryString = "call sp_addUser(" + req.userType.toString() + ",'" + req.username + "'," + "'" + req.password + "');";
        io:println(qryString);
        var ret = dbClient->select(qryString, Storage_RESULT);
        if ret is table<Storage_RESULT> {
            foreach var row in ret {
                if row.RESULT == "ok" {
                    io:println("user added");
                    return ();
                } else {
                    return error(row.RESULT);
                }
            }
        } else if ret is error {
            return ret;
        }
    }

    public function login(AddUserRequest req) returns @tainted ()|error {
        string qryString = "call sp_login(" + req.userType.toString() + ",'" + req.username + "'," + "'" + req.password + "');";
        io:println(qryString);
        var ret = dbClient->select(qryString, Storage_RESULT);
        if ret is table<Storage_RESULT> {
            foreach var row in ret {
                if row.RESULT == "ok" {
                    io:println("user added");
                    return ();
                } else {
                    return error(row.RESULT);
                }
            }
        } else if ret is error {
            return ret;
        }
    }
};
