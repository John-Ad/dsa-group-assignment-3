import ballerina/crypto;
import ballerina/io;
import ballerina/kafka;
import ballerina/lang.'string;

//-----------------------------------------------------------
//                DB SETUP
//-----------------------------------------------------------
DB_QryHandler dbConn = new ();


//-----------------------------------------------------------
//                PRODUCER SETUP
//-----------------------------------------------------------
kafka:ProducerConfiguration producerConfigs = {
    bootstrapServers: "192.168.49.1:9092",
    clientId: "outline_service",
    acks: "all",
    retryCount: 3
};

kafka:Producer producer = new (producerConfigs);
//-----------------------------------------------------------
//
//
//
//
//-----------------------------------------------------------
//                CONSUMER CONFIG
//-----------------------------------------------------------
string[] topics = [
        "outline_service"
    ];

kafka:ConsumerConfiguration consumerConfig = {
    bootstrapServers: "192.168.49.1:9092",
    groupId: "outline_service",
    topics: topics,
    pollingIntervalInMillis: 1000
};
//-----------------------------------------------------------
//
//
//
//
//-----------------------------------------------------------
//                CONSUMER SERVICE
//-----------------------------------------------------------
listener kafka:Consumer consumer = new (consumerConfig);

service KafkaRequestService on consumer {
    function __init() {
        var test = verifySignature("hello", "olleh");
        io:println(test);
    }
    resource function onMessage(any consumerAction, kafka:ConsumerRecord[] records) {
        // Dispatched set of Kafka records to service, We process each one by one.
        foreach var kafkaRecord in records {
            string|() msg = processKafkaRecord(kafkaRecord);
            if msg is string {
                error? _err = processRequest(msg);
            }
        }
    }
}
//-----------------------------------------------------------
//
//
//
//
//-----------------------------------------------------------
//                PROCESS KAFKA RECORDS
//-----------------------------------------------------------
public function processKafkaRecord(kafka:ConsumerRecord kafkaRecord) returns string|() {
    byte[] serializedMsg = <byte[]>kafkaRecord.value;
    string|error msg = 'string:fromBytes(serializedMsg);
    if (msg is string) {
        // Print the retrieved Kafka record.
        // io:println("Topic: ", kafkaRecord.topic, " Received Message: ", msg);
        return msg;
    } else {
        io:println("Error occurred while converting message data", msg);
    }
    return "";
}
//-----------------------------------------------------------
//
//
//
//
//-----------------------------------------------------------
//                PROCESS REQUEST
//-----------------------------------------------------------
function processRequest(string message) returns @tainted error? {

    io:println("request received");

    //----------------------------------
    //     CONVERT STRING TO OBJECT
    //----------------------------------
    any|error request = fromJsonString(message, TYPE_REQ);

    //----------------------------------
    //     CHECK IF ERROR OCCURED
    //----------------------------------
    if request is error {
        io:println(request.detail());
        return request;
    }
    if request is Request {

        //--------------------------------------------
        //     PASS TO REQUEST HANDLER FUNCTIONS
        //--------------------------------------------

        if request.reqType == TYPE_ADD_OUTLINE_REQ {            //      ADD OUTLINE
            addOutline(request.data);
            return;
        }

        if request.reqType == TYPE_APPROVE_OUTLINE_REQ {            //      APPROVE OUTLINE
            approveOutline(request.data);
            return;
        }

        if request.reqType == TYPE_GET_OUTLINE_REQ {            //      GET OUTLINE
            getOutline(request.data);
            return;
        }

        if request.reqType == TYPE_ACK_OUTLINE_REQ {            //      ACK OUTLINE
            ackOutline(request.data);
            return;
        }

        if request.reqType == TYPE_GET_ALL_OUTLINES_REQ {            //      GET ALL OUTLINES
            getAllOutlines(request.data);
            return;
        }

        if request.reqType == TYPE_GET_OUTLINES_BY_LECTURER_REQ {            //      GET OUTLINES BY LECTURER
            getOutlinesByLecturer(request.data);
            return;
        }
    }
}
//-----------------------------------------------------------
//
//
//
//
//-----------------------------------------------------------
//                PRODUCE MESSAGE
//-----------------------------------------------------------
function produceMessage(string topic, any data) {

    //-----------------------------------------
    //         CONVERT DATA TO JSON
    //-----------------------------------------
    json|error dataJson = toJson(data);

    if dataJson is json {

        //-----------------------------------------
        //         CONVERT JSON TO STRING
        //         THEN TO BYTES
        //-----------------------------------------
        string msg = dataJson.toJsonString();
        byte[] msgBytes = msg.toBytes();

        //-----------------------------------------
        //         SEND MESSAGE
        //-----------------------------------------
        var sendResult = producer->send(msgBytes, topic);
        if sendResult is error {
            io:println("failed to send message: ", sendResult);
        } else {
            io:println("successfully sent message");
        }

    } else {
        io:println("error sending message: ", dataJson.detail());
    }
}
//-----------------------------------------------------------
//
//
//
//
//
//
//-----------------------------------------------------------
//                ADD OUTLINE
//-----------------------------------------------------------
function addOutline(string data) {

    io:println("add outline request received");

    //-------------------------------
    //         CONVERT TO OBJ
    //-------------------------------
    any|error addOutlineData = fromJsonString(data, TYPE_ADD_OUTLINE_REQ);

    //-------------------------------
    //         CHECK FOR ERROR
    //-------------------------------
    if addOutlineData is error {
        io:println("error converting add outline obj: ", addOutlineData.detail());
        return;
    }
    if addOutlineData is AddOutlineRequest {
        io:println("add outline object valid");

        //------------------------------------------------------
        //         CREATE RESPONSE OBJ WITH SUCCESS PARAMS
        //------------------------------------------------------
        Response res = {
            status: 0,
            data: "ok"
        };

        //-------------------------------
        //    VERIFY LECTURER SIGNATURE
        //-------------------------------
        string dataToVerify = addOutlineData.courseName + addOutlineData.lecturerUsername;
        boolean|error sigVerified = verifySignature(dataToVerify, addOutlineData.signature);

        if sigVerified is error {
            io:println("error verifying sig: ", sigVerified.detail());
            res.status = 1;
            res.data = sigVerified.reason();
        } else {
            if !sigVerified {
                res.status = 1;
                res.data = "failed to verify lecturer signature";
                io:println("signature verification failed");
            } else {
                //-------------------------------
                //         PERFORM QRY
                //-------------------------------
                ()|error err = dbConn.addOutline(<@untained>addOutlineData);

                //--------------------------------------------
                //         SET RES TO ERROR IF ERR OCCURS
                //--------------------------------------------
                if err is error {
                    res.status = 1;
                    res.data = err.reason();
                    io:println(err);
                    io:println(err.detail());
                }
            }
        }

        io:println("sending message");
        //-------------------------------
        //         SEND RES
        //-------------------------------
        produceMessage(addOutlineData.clientTopic, res);
    }
}
//-----------------------------------------------------------
//
//
//
//
//-----------------------------------------------------------
//                GET OUTLINE
//-----------------------------------------------------------
function getOutline(string data) {

    //-------------------------------
    //         CONVERT TO OBJ
    //-------------------------------
    any|error getOutlineData = fromJsonString(data, TYPE_GET_OUTLINE_REQ);

    //-------------------------------
    //         CHECK FOR ERROR
    //-------------------------------
    if getOutlineData is error {
        io:println(getOutlineData.detail());
        return;
    }
    if getOutlineData is GetOutlineRequest {

        //------------------------------------------------------
        //         CREATE RESPONSE OBJ WITH SUCCESS PARAMS
        //------------------------------------------------------
        Response res = {
            status: 0,
            data: ""
        };

        //-------------------------------
        //         PERFORM QRY
        //-------------------------------
        Outline|error err = dbConn.getOutline(<@untained>getOutlineData);
        // io:println(err);

        //--------------------------------------------
        //         SET RES TO ERROR IF ERR OCCURS
        //--------------------------------------------
        if err is error {
            res.status = 1;
            res.data = err.reason();
            io:println(err);
            io:println(err.detail());
        } else {
            json|error resJson = toJson(err);
            if resJson is json {
                res.data = resJson.toJsonString();
            // io:println(resJson);
            } else {
                res.status = 1;
                res.data = resJson.reason();
            }
        }


        //-------------------------------
        //         SEND RES
        //-------------------------------
        produceMessage(getOutlineData.clientTopic, res);
    }
}
//-----------------------------------------------------------
//
//
//
//
//-----------------------------------------------------------
//                GET ALL OUTLINES
//-----------------------------------------------------------
function getAllOutlines(string data) {

    io:println("getting all outlines");

    //-------------------------------
    //         CONVERT TO OBJ
    //-------------------------------
    any|error getOutlineData = fromJsonString(data, TYPE_GET_OUTLINES_REQ);

    //-------------------------------
    //         CHECK FOR ERROR
    //-------------------------------
    if getOutlineData is error {
        io:println(getOutlineData.detail());
        return;
    }
    if getOutlineData is GetOutlinesRequest {

        //------------------------------------------------------
        //         CREATE RESPONSE OBJ WITH SUCCESS PARAMS
        //------------------------------------------------------
        Response res = {
            status: 0,
            data: ""
        };

        //-------------------------------
        //         PERFORM QRY
        //-------------------------------
        Outline[]|error err = dbConn.getAllOutlines(<@untained>getOutlineData);
        // io:println(err);

        //--------------------------------------------
        //         SET RES TO ERROR IF ERR OCCURS
        //--------------------------------------------
        if err is error {
            res.status = 1;
            res.data = err.reason();
            io:println(err);
            io:println(err.detail());
        } else {
            json|error resJson = json.constructFrom(err);
            if resJson is json {
                res.data = resJson.toJsonString();
            // io:println(resJson);
            } else {
                res.status = 1;
                res.data = resJson.reason();
            }
        }

        //-------------------------------
        //         SEND RES
        //-------------------------------
        produceMessage(getOutlineData.clientTopic, res);
    }
}
//-----------------------------------------------------------
//
//
//
//
//-----------------------------------------------------------
//                GET ALL OUTLINES
//-----------------------------------------------------------
function getOutlinesByLecturer(string data) {

    io:println("getting outlines by lecturer");

    //-------------------------------
    //         CONVERT TO OBJ
    //-------------------------------
    any|error getOutlineData = fromJsonString(data, TYPE_GET_OUTLINES_REQ);

    //-------------------------------
    //         CHECK FOR ERROR
    //-------------------------------
    if getOutlineData is error {
        io:println(getOutlineData.detail());
        return;
    }
    if getOutlineData is GetOutlinesRequest {

        //------------------------------------------------------
        //         CREATE RESPONSE OBJ WITH SUCCESS PARAMS
        //------------------------------------------------------
        Response res = {
            status: 0,
            data: ""
        };

        //-------------------------------
        //         PERFORM QRY
        //-------------------------------
        Outline[]|error err = dbConn.getOutlinesByLecturer(<@untained>getOutlineData);
        // io:println(err);

        //--------------------------------------------
        //         SET RES TO ERROR IF ERR OCCURS
        //--------------------------------------------
        if err is error {
            res.status = 1;
            res.data = err.reason();
            io:println(err);
            io:println(err.detail());
        } else {
            json|error resJson = json.constructFrom(err);
            if resJson is json {
                res.data = resJson.toJsonString();
            // io:println(resJson);
            } else {
                res.status = 1;
                res.data = resJson.reason();
            }
        }

        //-------------------------------
        //         SEND RES
        //-------------------------------
        produceMessage(getOutlineData.clientTopic, res);
    }
}
//
//
//
//
//-----------------------------------------------------------
//                APPROVE OUTLINE
//-----------------------------------------------------------
function approveOutline(string data) {

    //-------------------------------
    //         CONVERT TO OBJ
    //-------------------------------
    any|error approveOutlineData = fromJsonString(data, TYPE_APPROVE_OUTLINE_REQ);

    //-------------------------------
    //         CHECK FOR ERROR
    //-------------------------------
    if approveOutlineData is error {
        io:println(approveOutlineData.detail());
        return;
    }
    if approveOutlineData is ApproveOutlineRequest {

        //------------------------------------------------------
        //         CREATE RESPONSE OBJ WITH SUCCESS PARAMS
        //------------------------------------------------------
        Response res = {
            status: 0,
            data: "ok"
        };

        //-------------------------------
        //      VERIFY HOD SIG
        //-------------------------------
        string dataToVerify = approveOutlineData.courseName + approveOutlineData.hodUsername;
        boolean|error sigVerified = verifySignature(dataToVerify, approveOutlineData.signature);

        if sigVerified is error {
            res.status = 1;
            res.data = sigVerified.reason();
            io:println("failed to verify hod sig");
        } else {

            io:println("hod sig verified");

            //-------------------------------
            //      PERFORM APPROVE QRY
            //-------------------------------
            ()|error err = dbConn.approveOutline(<@untained>approveOutlineData);

            //--------------------------------------------
            //      SET RES TO ERROR IF ERR OCCURS
            //--------------------------------------------
            if err is error {
                res.status = 1;
                res.data = err.reason();
                io:println(err);
                io:println(err.detail());
            }
        }

        //-------------------------------
        //         SEND RES
        //-------------------------------
        produceMessage(approveOutlineData.clientTopic, res);
    }
}
//-----------------------------------------------------------
//
//
//
//
//-----------------------------------------------------------
//                ACKNOWLEDGE OUTLINE
//-----------------------------------------------------------
function ackOutline(string data) {

    //-------------------------------
    //         CONVERT TO OBJ
    //-------------------------------
    any|error ackOutlineData = fromJsonString(data, TYPE_ACK_OUTLINE_REQ);

    //-------------------------------
    //         CHECK FOR ERROR
    //-------------------------------
    if ackOutlineData is error {
        io:println(ackOutlineData.detail());
        return;
    }
    if ackOutlineData is AckOutlineRequest {

        //------------------------------------------------------
        //         CREATE RESPONSE OBJ WITH SUCCESS PARAMS
        //------------------------------------------------------
        Response res = {
            status: 0,
            data: "ok"
        };

        //-------------------------------
        //         PERFORM QRY
        //-------------------------------
        ()|error err = dbConn.ackOutline(<@untained>ackOutlineData);

        //--------------------------------------------
        //         SET RES TO ERROR IF ERR OCCURS
        //--------------------------------------------
        if err is error {
            res.status = 1;
            res.data = err.reason();
            io:println(err);
            io:println(err.detail());
        }

        //-------------------------------
        //         SEND RES
        //-------------------------------
        produceMessage(ackOutlineData.clientTopic, res);
    }
}
//-----------------------------------------------------------
//
//
//
//
//-----------------------------------------------------------
//                GENERATE SIGNATURE
//-----------------------------------------------------------
function generateSignature(string data) returns string|error {
    io:println("generating signature");

    //-------------------------------
    //       SET KEYSTORE DETAILS
    //-------------------------------
    crypto:KeyStore ks = {
        path: "/home/ballerina/keys/keystore.p12",
        password: "admin"
    };

    //--------------------------------------
    //     READ PRIVATE KEY FROM KEYSTORE
    //--------------------------------------
    var privateKey = crypto:decodePrivateKey(ks, "1", "admin");
    io:println("reached: ", privateKey);

    //-------------------------------
    //     CHECK FOR ERROR
    //-------------------------------
    if privateKey is crypto:PrivateKey {

        io:println("signing data");

        //-------------------------------
        //         SIGN DATA
        //-------------------------------
        byte[]|error signature = crypto:signRsaSha256(data.toBytes(), privateKey);

        if signature is byte[] {

            io:println("successfully signed data");

            //-------------------------------
            //   RETURN SIG IN HEX FORMAT
            //-------------------------------
            return signature.toBase16();

        } else {

            io:println("failed to sign data");
            return signature;

        }
    } else {
        io:println("error reading key data: ", privateKey.detail());
        return privateKey;
    }
}
//-----------------------------------------------------------
//
//
//
//
//-----------------------------------------------------------
//                VERIFY SIGNATURE
//-----------------------------------------------------------
function verifySignature(string data, string signature) returns boolean|error {
    io:println("verifying signature");

    //------------------------------------------------------------------
    //       REGENERATE SIGNATURE USING THE PRIVATE STORED IN SERVER
    //------------------------------------------------------------------
    string|error sigToCompare = generateSignature(data);

    //-------------------------------
    //       CHECK FOR ERROR
    //-------------------------------
    if sigToCompare is string {

        //--------------------------------------
        //   VERIFY SIG AND RETURN TRUE/FALSE
        //--------------------------------------
        if signature == sigToCompare {
            return true;
        }
        return false;

    } else {
        return error("failed to regenerate signature");
    }
}
//-----------------------------------------------------------



