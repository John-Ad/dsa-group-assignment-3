import ballerina/io;
import ballerinax/java.jdbc;

jdbc:Client dbClient = new ({
    url: "jdbc:mysql://192.168.49.1:3306/DSA_3_DB",
    username: "dev",
    password: "Dev@220038",
    dbOptions: {useSSL: false, allowPublicKeyRetrieval: true}
});

type TestRecord record {
    string RESULT;
};

public type DB_QryHandler object {

    public function __init() {
        var ret = dbClient->select("select 'working' as RESULT", TestRecord);
        if ret is table<TestRecord> {
            foreach var row in ret {
                io:println(row.RESULT);
            }
        } else {
            io:println(ret.toString());
        }
    }

    public function getOutline(GetOutlineRequest req) returns @tainted Outline|error {
        string qryString = "call sp_getOutline(" + req.userType.toString() + ", '" + req.username + "', '" + req.courseName + "');";
        io:println(qryString);
        var ret = dbClient->select(qryString, Outline);
        if ret is table<Outline> {
            if ret.hasNext() {
                io:println("successfully retrieved outline from db");
                return ret.getNext();
            } else {
                io:println("no records found");
                return error("no records found");
            }
        } else {
            io:println("error while getting outline from db");
            return <error>ret;
        }
    }

    public function addOutline(AddOutlineRequest req) returns @tainted ()|error {
        string qryString = "call sp_addOutline('" + req.courseName + "', '" + req.lecturerUsername + "','" + req.signature + "', '" + req.learningOutcomes + "', '" + req.content + "', '" + req.courseSchedule + "', '" + req.assessmentSchedule + "');";
        io:println(qryString);
        var ret = dbClient->select(qryString, Storage_RESULT);
        if ret is table<Storage_RESULT> {
            foreach var row in ret {
                if row.RESULT == "ok" {
                    io:println("outline added");
                    return ();
                } else {
                    return error(row.RESULT);
                }
            }
        } else if ret is error {
            return ret;
        }
    }

    public function approveOutline(ApproveOutlineRequest req) returns @tainted ()|error {
        string qryString = "call sp_approveOutline('" + req.hodUsername + "', '" + req.signature + "','" + req.courseName + "');";
        io:println(qryString);
        var ret = dbClient->select(qryString, Storage_RESULT);
        if ret is table<Storage_RESULT> {
            foreach var row in ret {
                if row.RESULT == "ok" {
                    io:println("outline approved");
                    return ();
                } else {
                    return error(row.RESULT);
                }
            }
        } else if ret is error {
            return ret;
        }
    }

    public function ackOutline(AckOutlineRequest req) returns @tainted ()|error {
        string qryString = "call sp_ackOutline('" + req.studentUsername + "', '" + req.courseName + "');";
        io:println(qryString);
        var ret = dbClient->select(qryString, Storage_RESULT);
        if ret is table<Storage_RESULT> {
            foreach var row in ret {
                if row.RESULT == "ok" {
                    io:println("outline acknowledged");
                    return ();
                } else {
                    return error(row.RESULT);
                }
            }
        } else if ret is error {
            return ret;
        }
    }

    public function getOutlineSignatures(string courseName) returns @tainted OutlineSignatures|error {
        string qryString = "call sp_getOutlineSignatures('" + courseName + "');";
        io:println(qryString);
        var ret = dbClient->select(qryString, OutlineSignatures);
        if ret is table<OutlineSignatures> {
            if ret.hasNext() {
                return ret.getNext();
            } else {
                return error("no records found");
            }
        } else {
            return <error>ret;
        }
    }

    public function getAllOutlines(GetOutlinesRequest req) returns @tainted Outline[]|error {
        string qryString = "call sp_getAllOutlines('" + req.username + "');";
        io:println(qryString);
        var ret = dbClient->select(qryString, Outline);
        if ret is table<Outline> {
            if ret.hasNext() {
                io:println("successfully retrieved outline from db");
                Outline[] outlines = [];
                foreach var row in ret {
                    outlines.push(row);
                }
                return outlines;
            } else {
                io:println("no records found or user not authorized");
                return error("no records found or user not authorized");
            }
        } else {
            io:println("error while getting outline from db");
            return <error>ret;
        }
    }

    public function getOutlinesByLecturer(GetOutlinesRequest req) returns @tainted Outline[]|error {
        string qryString = "call sp_getOutlinesByLecturer('" + req.username + "');";
        io:println(qryString);
        var ret = dbClient->select(qryString, Outline);
        if ret is table<Outline> {
            if ret.hasNext() {
                io:println("successfully retrieved outline from db");
                Outline[] outlines = [];
                foreach var row in ret {
                    outlines.push(row);
                }
                return outlines;
            } else {
                io:println("no records found");
                return error("no records found");
            }
        } else {
            io:println("error while getting outline from db");
            return <error>ret;
        }
    }
};
