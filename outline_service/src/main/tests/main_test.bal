import ballerina/io;
import ballerina/test;

# Before Suite Function

@test:BeforeSuite
function beforeSuiteFunc() {
// io:println("I'm the before suite function!");
}

# Before test function

function beforeFunc() {
// io:println("I'm the before function!");
}

# Test function

// @test:Config {
// }
// function testAddOutline() {

//     io:println("#####################################");
//     io:println("######   TESTING ADD OUTLINE   ######");
//     io:println("#####################################");

//     AddOutlineRequest req = {
//         clientTopic: "cl",
//         courseName: "dsa",
//         lecturerUsername: "john",
//         signature: "",
//         learningOutcomes: "learn stuff \n become knowledgable on other stuff \n fail to meet objectives",
//         content: "why even try, youre just going to fail anyway",
//         courseSchedule: "week1: fail \n week2: cry \n week3: kys",
//         assessmentSchedule: "assignment1: due tomorrow \n assignment2: due last week"
//     };

//     json|error reqJS = toJson(req);

//     if reqJS is json {
//         string jsStr = reqJS.toJsonString();
//         addOutline(jsStr);
//     }

//     test:assertTrue(true, msg = "Failed!");
// }

@test:Config {
}
function testApproveOutline() {

    io:println("#########################################");
    io:println("######   TESTING APPROVE OUTLINE   ######");
    io:println("#########################################");

    ApproveOutlineRequest req = {
        clientTopic: "cl",
        hodUsername: "john",
        signature: "",
        courseName: "dsa"
    };

    json|error reqJS = toJson(req);

    if reqJS is json {
        string jsStr = reqJS.toJsonString();
        OutlineSignatures|error outlineSignatures = dbConn.getOutlineSignatures("DSA");
        io:println(outlineSignatures);
    }

    test:assertTrue(true, msg = "Failed!");
}

// @test:Config {
// }
// function testAckOutline() {

//     io:println("#########################################");
//     io:println("######   TESTING ACK OUTLINE       ######");
//     io:println("#########################################");

//     AckOutlineRequest req = {
//         clientTopic: "cl",
//         studentUsername: "john",
//         courseName: "dsa"
//     };

//     json|error reqJS = toJson(req);

//     if reqJS is json {
//         string jsStr = reqJS.toJsonString();
//         ackOutline(jsStr);
//     }

//     test:assertTrue(true, msg = "Failed!");
// }

// @test:Config {
// }
// function testGetOutline() {

//     io:println("#########################################");
//     io:println("######   TESTING GET OUTLINE       ######");
//     io:println("#########################################");

//     GetOutlineRequest req = {
//         clientTopic: "cl",
//         userType: 1,        // HOD
//         username: "rg",
//         courseName: "ICE"
//     };

//     json|error reqJS = toJson(req);

//     if reqJS is json {
//         string jsStr = reqJS.toJsonString();
//         getOutline(jsStr);
//     }

//     test:assertTrue(true, msg = "Failed!");
// }

# After test function

function afterFunc() {
// io:println("I'm the after function!");
}

# After Suite Function

@test:AfterSuite
function afterSuiteFunc() {
// io:println("I'm the after suite function!");
}
