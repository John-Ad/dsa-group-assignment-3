import ballerina/io;


//----------------------------
//     REQUEST OBJECT
//----------------------------
public type Request record {
    int reqType;
    string data;
};

//----------------------------
//     RESPONSE OBJECT
//----------------------------
public type Response record {
    int status;
    string data;
};

//----------------------------
//     STORAGE POST RESULT
//----------------------------
public type Storage_RESULT record {
    string RESULT;
};


//-------------------------
//     OUTLINE OBJECT
//-------------------------
public type Outline record {
    string courseName;
    string lecturerUsername;
    string learningOutcomes;
    string content;
    string courseSchedule;
    string assessmentSchedule;
    int approved;
};


//-----------------------------
//     ADD OUTLINE REQUEST
//-----------------------------
public type AddOutlineRequest record {
    string clientTopic;
    string courseName;
    string lecturerUsername;
    string signature;
    string learningOutcomes;
    string content;
    string courseSchedule;
    string assessmentSchedule;
};


//-----------------------------
//     GET OUTLINE REQUEST
//-----------------------------
public type GetOutlineRequest record {
    string clientTopic;
    int userType;
    string courseName;
    string username;
};

//-----------------------------
//     GET OUTLINES REQUEST
//-----------------------------
public type GetOutlinesRequest record {
    string clientTopic;
    string username;
};


//--------------------------------
//     APPROVE OUTLINE REQUEST
//--------------------------------
public type ApproveOutlineRequest record {
    string clientTopic;
    string hodUsername;
    string signature;
    string courseName;
};


//------------------------------------
//     ACKNOWLEDGE OUTLINE REQUEST
//------------------------------------
public type AckOutlineRequest record {
    string clientTopic;
    string studentUsername;
    string courseName;
};


//------------------------------------
//     OUTLINE SIGNATURES OBJECT
//------------------------------------
public type OutlineSignatures record {
    string lectUsername;
    string lectSignature;
    string hodSignature;
};


//---------------------------------------------------
//          CONVERT OBJ TO JSON
//---------------------------------------------------
public function toJson(any data) returns json|error {
    if data is Response {
        return {
            status: data.status,
            data: data.data
        };
    }
    if data is Request {
        return {
            reqType: data.reqType,
            data: data.data
        };
    }
    if data is AddOutlineRequest {
        return {
            clientTopic: data.clientTopic,
            courseName: data.courseName,
            signature: data.signature,
            lecturerUsername: data.lecturerUsername,
            learningOutcomes: data.learningOutcomes,
            content: data.content,
            courseSchedule: data.courseSchedule,
            assessmentSchedule: data.assessmentSchedule
        };
    }
    if data is Outline {
        return {
            courseName: data.courseName,
            lecturerUsername: data.lecturerUsername,
            learningOutcomes: data.learningOutcomes,
            content: data.content,
            courseSchedule: data.courseSchedule,
            assessmentSchedule: data.assessmentSchedule,
            approved: data.approved
        };
    }
    if data is ApproveOutlineRequest {
        return {
            clientTopic: data.clientTopic,
            hodUsername: data.hodUsername,
            signature: data.signature,
            courseName: data.courseName
        };
    }
    if data is AckOutlineRequest {
        return {
            clientTopic: data.clientTopic,
            studentUsername: data.studentUsername,
            courseName: data.courseName
        };
    }
    if data is GetOutlineRequest {
        return {
            clientTopic: data.clientTopic,
            userType: data.userType,
            courseName: data.courseName,
            username: data.username
        };
    }
    if data is GetOutlinesRequest {
        return {
            clientTopic: data.clientTopic,
            username: data.username
        };
    }

    return error("public type not recognized");
}


public const TYPE_REQ = 0;
public const TYPE_RES = 1;
public const TYPE_OUTLINE = 7;
public const TYPE_ADD_OUTLINE_REQ = 8;
public const TYPE_GET_OUTLINE_REQ = 9;
public const TYPE_APPROVE_OUTLINE_REQ = 10;
public const TYPE_ACK_OUTLINE_REQ = 11;
public const TYPE_GET_OUTLINES_REQ = 17;
public const TYPE_GET_ALL_OUTLINES_REQ = 18;
public const TYPE_GET_OUTLINES_BY_LECTURER_REQ = 19;


//---------------------------------------------------
//          CONVERT JSON TO OBJ
//---------------------------------------------------
public function fromJsonString(string jsonStr, int convType) returns @tainted any|error {
    io:StringReader sr = new (jsonStr, "UTF-8");
    json|error jObj = sr.readJson();

    if jObj is error {
        return jObj;
    } else {
        if convType == TYPE_RES {
            return Response.constructFrom(jObj);
        }
        if convType == TYPE_REQ {
            return Request.constructFrom(jObj);
        }
        if convType == TYPE_OUTLINE {
            return Outline.constructFrom(jObj);
        }
        if convType == TYPE_ADD_OUTLINE_REQ {
            return AddOutlineRequest.constructFrom(jObj);
        }
        if convType == TYPE_GET_OUTLINE_REQ {
            return GetOutlineRequest.constructFrom(jObj);
        }
        if convType == TYPE_GET_OUTLINES_REQ {
            return GetOutlinesRequest.constructFrom(jObj);
        }
        if convType == TYPE_APPROVE_OUTLINE_REQ {
            return ApproveOutlineRequest.constructFrom(jObj);
        }
        if convType == TYPE_ACK_OUTLINE_REQ {
            return AckOutlineRequest.constructFrom(jObj);
        }
    }
}
