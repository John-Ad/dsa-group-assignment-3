echo "############################"
echo "DEPLOYING OUTLINE SERVICE"
echo "############################"

bal build --skip-tests main
cp ./target/bin/main.jar ./docker/
cd docker
eval $(minikube -p minikube docker-env)
docker build . -t outline-service:latest
cd ..
helm uninstall outline-service-deployment
helm install outline-service-deployment ./outline-service-deployment