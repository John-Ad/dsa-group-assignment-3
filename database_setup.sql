/* 
    EXECUTE: source /home/john/Documents/Computer-Science-Semester-4/Distributed-Programming/dsa-group-assignment-3/database_setup.sql;
*/

drop database DSA_3_DB;
create database DSA_3_DB;
use DSA_3_DB;

create table Hod(
    Hod_Username varchar(100) primary key,
    Hod_Password varchar(100) not null
);
create table Student(
    Student_Username varchar(100) primary key,
    Student_Password varchar(100) not null
);
create table Lecturer(
    Lecturer_Username varchar(100) primary key,
    Lecturer_Password varchar(100) not null
);
create table Course(
    Course_Name varchar(100) primary key,
    Lecturer_Username varchar(100) not null,
    Course_Desc varchar(500),

    foreign key (Lecturer_Username) references Lecturer(Lecturer_Username)
);
create table Student_Course(
    Student_Username varchar(100),
    Course_Name varchar(100),

    foreign key (Student_Username) references Student(Student_Username),
    foreign key (Course_Name) references Course(Course_Name),

    primary key (Student_Username, Course_Name)
);
create table Course_Outline(
    Course_Name varchar(100) primary key,
    Lecturer_Username varchar(100) not null,
    Outline_Signature varchar(1000) not null,
    Outline_Learning_Outcomes varchar(1000),
    Outline_Content varchar(3000),
    Outline_Course_Schedule varchar(1000),
    Outline_Assessment_Schedule varchar(5000), 
    Outline_Approved_Signature varchar(1000),
    Outline_Approved bit not null,

    foreign key (Course_Name) references Course(Course_Name),
    foreign key (Lecturer_Username) references Lecturer(Lecturer_Username)
);
create table Ack_Outline(
    Course_Name varchar(100),
    Student_Username varchar(100),

    foreign key (Course_Name) references Course(Course_Name),
    foreign key (Student_Username) references Student(Student_Username),
    primary key(Course_Name, Student_Username)
);

/*-----------------------------------------*/
/*         ADD USER*/
/*-----------------------------------------*/
delimiter //
create procedure sp_addUser(
    in uType int,
    in username varchar(100),
    in pword varchar(100)
)
begin
    if(uType=1) then
        if(username not in(select Hod_Username from Hod)) then
            insert into Hod(Hod_Username,Hod_Password) values(username,pword);
            select 'ok' as RESULT;
        else
            select 'hod already exists' as RESULT;
        end if;
    else
        if(uType=2) then
            if(username not in(select Student_Username from Student)) then
                insert into Student(Student_Username,Student_Password) values(username,pword);
                select 'ok' as RESULT;
            else
                select 'student already exists' as RESULT;
            end if;
        else
            if(uType=3) then
                if(username not in(select Lecturer_Username from Lecturer)) then
                    insert into Lecturer(Lecturer_Username,Lecturer_Password) values(username,pword);
                    select 'ok' as RESULT;
                else
                    select 'lecturer already exists' as RESULT;
                end if;
            end if;
        end if;
    end if;
end //
delimiter ;


/*-----------------------------------------*/
/*         ADD COURSE*/
/*-----------------------------------------*/
delimiter //
create procedure sp_addCourse(
    in hodUname varchar(100),
    in cName varchar(100),
    in lName varchar(100),
    in cDesc varchar(500)
)
begin
    if(hodUname in(select Hod_Username from Hod) AND cName not in(select Course_Name from Course) AND lName in(select Lecturer_Username from Lecturer)) then
        insert into Course(Course_Name,Lecturer_Username,Course_Desc) values(cName,lName,cDesc);
        select 'ok' as RESULT;
    else
        select 'course already exists, or lecturer or hod does not exist' as RESULT;
    end if;
end //
delimiter ;

/*-----------------------------------------*/
/*         UPDATE COURSE*/
/*-----------------------------------------*/
delimiter //
create procedure sp_updateCourse(
    in uname varchar(100),
    in cname varchar(100),
    in cDesc varchar(500)
)
begin
    if(uname in(select Hod_Username from Hod) OR  uname in(select Lecturer_Username from Lecturer)) then
        update Course
        set Course_Desc=cDesc
        where Course_Name=cname;

        select 'ok' as RESULT;
    else
        select 'user not authorized' as RESULT;
    end if;
end //
delimiter ;


/*-----------------------------------------*/
/*         ADD STUDENT COURSE*/
/*-----------------------------------------*/
delimiter //
create procedure sp_addStudentCourse(
    in sName varchar(100),
    in cName varchar(100)
)
begin
    if(cName in(select Course_Name from Course) AND sName in(select Student_Username from Student) AND cName not in(select Course_Name from Student_Course where Student_Username=sName)) then
        insert into Student_Course(Student_Username,Course_Name) values(sName,cName);
        select 'ok' as RESULT;
    else
        select 'course or student doesnt exists, or student is already registered for course' as RESULT;
    end if;
end //
delimiter ;

/*-----------------------------------------*/
/*         VIEW STUDENT COURSES*/
/*-----------------------------------------*/
delimiter //
create procedure sp_getStudentCourses(
    in sName varchar(100)
)
begin
    select Course.Course_Name, Course.Lecturer_Username, Course.Course_Desc 
    from Student_Course 
    inner join Course
    on Course.Course_Name=Student_Course.Course_Name 
    where Student_Course.Student_Username=sName;
end //
delimiter ;


/*-----------------------------------------*/
/*         ADD OUTLINE*/
/*-----------------------------------------*/
delimiter //
create procedure sp_addOutline(
    cName varchar(100),
    lUsername varchar(100),
    oSignature varchar(1000),
    oLearningOutcomes varchar(1000),
    oContent varchar(3000),
    oCourseSchedule varchar(5000),
    oAssessmentSchedule varchar(5000) 
)
begin
    if(cName in(select Course_Name from Course) AND lUsername in(select Lecturer_Username from Lecturer) AND cName not in(select Course_Name from Course_Outline)) then
        insert into Course_Outline(Course_Name, Lecturer_Username, Outline_Signature, Outline_Learning_Outcomes, Outline_Content, Outline_Course_Schedule, Outline_Assessment_Schedule, Outline_Approved_Signature, Outline_Approved)
        values(cName, lUsername, oSignature, oLearningOutcomes, oContent, oCourseSchedule, oAssessmentSchedule, "", 0);
        select 'ok' as RESULT;
    else
        select 'course outline already exists, or course or lecturer doesnt exist' as RESULT;
    end if;
end //
delimiter ;


/*-----------------------------------------*/
/*         APPROVE OUTLINE*/
/*-----------------------------------------*/
delimiter //
create procedure sp_approveOutline(
    hName varchar(100),
    hSig varchar(1000),
    cName varchar(100)
)
begin
    if(hName in(select Hod_Username from Hod) AND cName in(select Course_Name from Course) AND cName in(select Course_Name from Course_Outline)) then
        update Course_Outline
        set Outline_Approved_Signature=hSig, Outline_Approved=1
        where Course_Name=cName;

        select 'ok' as RESULT;
    else
        select 'course or hod doesnt exists' as RESULT;
    end if;
end //
delimiter ;


/*-----------------------------------------*/
/*         ACK OUTLINE*/
/*-----------------------------------------*/
delimiter //
create procedure sp_ackOutline(
    sName varchar(100),
    cName varchar(100)
)
begin
    if(sName in(select Student_Username from Student) AND cName in(select Course_Name from Course)) then
        if(sName not in(select Student_Username from Ack_Outline where Course_Name=cName)) then
            insert into Ack_Outline(Course_Name, Student_Username) values(cName,sName);
            select 'ok' as RESULT;
        else
            select 'outline already acknowledged' as RESULT;
        end if;
    else
        select 'course or student doesnt exists' as RESULT;
    end if;
end //
delimiter ;

/*-----------------------------------------*/
/*         GET OUTLINE*/
/*-----------------------------------------*/
delimiter //
create procedure sp_getOutline(
    uType int,
    uName varchar(100),
    cName varchar(100)
)
begin
    if(uType=1) then   /*HOD*/
        select Course_Outline.Course_Name, Course_Outline.Lecturer_Username, Course_Outline.Outline_Learning_Outcomes, Course_Outline.Outline_Content, Course_Outline.Outline_Course_Schedule, Course_Outline.Outline_Assessment_Schedule, Course_Outline.Outline_Approved from Course_Outline
        where Course_Name=cName;
    else
        if(uType=2) then   /*STUDENT*/
            select Course_Outline.Course_Name, Course_Outline.Lecturer_Username, Course_Outline.Outline_Learning_Outcomes, Course_Outline.Outline_Content, Course_Outline.Outline_Course_Schedule, Course_Outline.Outline_Assessment_Schedule, Course_Outline.Outline_Approved from Course_Outline
            inner join Student_Course
            on Student_Course.Course_Name=Course_Outline.Course_Name
            where Student_Course.Student_Username=uName AND Course_Outline.Course_Name=cName;
        else 
            if(uType=3) then  /*LECTURER*/
                select Course_Outline.Course_Name, Course_Outline.Lecturer_Username, Course_Outline.Outline_Learning_Outcomes, Course_Outline.Outline_Content, Course_Outline.Outline_Course_Schedule, Course_Outline.Outline_Assessment_Schedule, Course_Outline.Outline_Approved from Course_Outline
                where Lecturer_Username=uName AND Course_Name=cName;
            end if;
        end if;
    end if;
end //
delimiter ;

/*-----------------------------------------*/
/*         GET OUTLINES BY LECTURER*/
/*-----------------------------------------*/
delimiter //
create procedure sp_getOutlinesByLecturer(
    uName varchar(100)
)
begin
    select Course_Outline.Course_Name, Course_Outline.Lecturer_Username, Course_Outline.Outline_Learning_Outcomes, Course_Outline.Outline_Content, Course_Outline.Outline_Course_Schedule, Course_Outline.Outline_Assessment_Schedule, Course_Outline.Outline_Approved 
    from Course_Outline
    where Lecturer_Username=uName;
end //
delimiter ;

/*-----------------------------------------*/
/*         GET ALL OUTLINES*/
/*-----------------------------------------*/
delimiter //
create procedure sp_getAllOutlines(
    uName varchar(100)
)
begin
    if(uName in(select Hod_Username from Hod)) then
        select Course_Outline.Course_Name, Course_Outline.Lecturer_Username, Course_Outline.Outline_Learning_Outcomes, Course_Outline.Outline_Content, Course_Outline.Outline_Course_Schedule, Course_Outline.Outline_Assessment_Schedule, Course_Outline.Outline_Approved 
        from Course_Outline;
    else
        select Course_Outline.Course_Name, Course_Outline.Lecturer_Username, Course_Outline.Outline_Learning_Outcomes, Course_Outline.Outline_Content, Course_Outline.Outline_Course_Schedule, Course_Outline.Outline_Assessment_Schedule, Course_Outline.Outline_Approved 
        from Course_Outline
        where Course_Outline.Course_Name="UNAUTHORIZED ACCESS";
    end if;
end //
delimiter ;

/*-----------------------------------------*/
/*         GET OUTLINE SIGNATURES*/
/*-----------------------------------------*/
delimiter //
create procedure sp_getOutlineSignatures(
    cName varchar(100)
)
begin
    select Lecturer_Username as lectUsername, Outline_Signature as lectSignature, Outline_Approved_Signature as hodSignature
    from Course_Outline
    where Course_Name=cName; 
end //
delimiter ;

/*-----------------------------------------*/
/*         GET OUTLINE SIGNATURES*/
/*-----------------------------------------*/
delimiter //
create procedure sp_login(
    uType int,
    uName varchar(100),
    uPword varchar(100)
)
begin
    if(uType=1) then
        if(uPword in(select Hod_Password from Hod where Hod_Username=uName)) then
            select 'ok' as RESULT;
        else
            select 'incorrect login details' as RESULT;
        end if;
    else
        if(uType=2) then
            if(uPword in(select Student_Password from Student where Student_Username=uName)) then
                select 'ok' as RESULT;
            else
                select 'incorrect login details' as RESULT;
            end if;
        else
            if(uType=3) then
                if(uPword in(select Lecturer_Password from Lecturer where Lecturer_Username=uName)) then
                    select 'ok' as RESULT;
                else
                    select 'incorrect login details' as RESULT;
                end if;
            end if;
        end if;
    end if; 
end //
delimiter ;