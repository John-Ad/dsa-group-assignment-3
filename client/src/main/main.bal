import ballerina/crypto;
import ballerina/io;
import ballerina/kafka;
import ballerina/lang.'string;
import ballerina/lang.'int;
import ballerina/runtime;
import ballerina/system;

string clientID = system:uuid();
string[] responseList = [];

//-----------------------------------------------------------
//                PRODUCER SETUP
//-----------------------------------------------------------
kafka:ProducerConfiguration producerConfigs = {
    bootstrapServers: "localhost:9093",
    clientId: "client" + clientID,
    acks: "all",
    retryCount: 3
};

kafka:Producer producer = new (producerConfigs);
//-----------------------------------------------------------
//
//
//
//
//-----------------------------------------------------------
//                CONSUMER CONFIG
//-----------------------------------------------------------
string[] topics = [
        clientID
    ];

kafka:ConsumerConfiguration consumerConfig = {
    bootstrapServers: "localhost:9093",
    groupId: clientID,
    topics: topics,
    pollingIntervalInMillis: 1000
};
//-----------------------------------------------------------
//
//
//
//
//-----------------------------------------------------------
//                CONSUMER SERVICE
//-----------------------------------------------------------
kafka:Consumer consumer = new (consumerConfig);

service KafkaService = service {
    resource function onMessage(any consumerAction, kafka:ConsumerRecord[] records) {
        // Dispatched set of Kafka records to service, We process each one by one.
        foreach var kafkaRecord in records {
            string|() msg = processKafkaRecord(kafkaRecord);
            if msg is string {
                error? _err = processResponse(msg);
            }
        }
    }
};
//-----------------------------------------------------------
//
//
//
//
//----------------------------------------------------------------
//    MAIN FUNCTION
//----------------------------------------------------------------
public function main() {
    worker UI_LOOP {
        boolean running = true;
        string input = "";
        int|error option = 0;
        io:println("working");
        while running {

            //--------------------------------
            //    OPTIONS
            //--------------------------------
            io:println("1. Test adding user");
            io:println("2. Test user login");
            io:println("3. Test adding course");
            io:println("4. Test updating course");
            io:println("5. Test adding student course");
            io:println("6. Test getting student courses");
            io:println("7. Test adding outline");
            io:println("8. Test viewing single outline for student");
            io:println("9. Test viewing all outlines as hod");
            io:println("10. Test viewing all outlines by a lecturer");
            io:println("11. Test test approving outline");
            io:println("12. Test test acknowledging outline");
            io:println("13. Exit");

            //--------------------------------
            //    HANDLE OPTION INPUT
            //--------------------------------
            input = io:readln("Option: ");
            option = 'int:fromString(input);
            if option is error {
                io:println("\nPlease enter a valid number as an option!\n");
            } else {
                if option == 13 {
                    running = false;
                    io:println("Press ctrl+c to stop listener service");
                } else if option == 1 {
                    addUser();
                } else if option == 2 {
                    loginUser();
                } else if option == 3 {
                    addCourse();
                } else if option == 4 {
                    updateCourse();
                } else if option == 5 {
                    addStudentCourse();
                } else if option == 6 {
                    getStudentCourses();
                } else if option == 7 {
                    addOutline();
                } else if option == 8 {
                    getOutline();
                } else if option == 9 {
                    getAllOutlines();
                } else if option == 10 {
                    getOutlinesByLecturer();
                } else if option == 11 {
                    approveOutline();
                } else if option == 12 {
                    ackOutline();
                }
            }

        }
        return;
    }

    ()|error err = consumer.__attach(KafkaService);
    if err is error {
        io:println("failed to attach service to listener");
        panic err;
    } else {
        err = consumer.__start();
        if err is error {
            io:println("failed to start kafka listener, exiting: ", err.detail());
            UI_LOOP.cancel();
        }
    }
}
//-----------------------------------------------------------
//
//
//
//
//-----------------------------------------------------------
//                PROCESS KAFKA RECORDS
//-----------------------------------------------------------
public function processKafkaRecord(kafka:ConsumerRecord kafkaRecord) returns string|() {
    byte[] serializedMsg = <byte[]>kafkaRecord.value;
    string|error msg = 'string:fromBytes(serializedMsg);
    if (msg is string) {
        // Print the retrieved Kafka record.
        // io:println("Topic: ", kafkaRecord.topic, " Received Message: ", msg);
        return msg;
    } else {
        io:println("Error occurred while converting message data", msg);
    }
    return "";
}
//-----------------------------------------------------------
//
//
//
//
//-----------------------------------------------------------
//                PROCESS RESPONSE
//-----------------------------------------------------------
function processResponse(string message) returns @tainted error? {
    responseList.push(message);
}
//-----------------------------------------------------------
//
//
//
//
//-----------------------------------------------------------
//                 PRODUCE MESSAGE
//-----------------------------------------------------------
function produceMessage(string topic, any data, int numOfResponses) {

    //-----------------------------------------
    //         CONVERT DATA TO JSON
    //-----------------------------------------
    json|error dataJson = toJson(data);

    if dataJson is json {

        //-----------------------------------------
        //         CONVERT JSON TO STRING
        //         THEN TO BYTES
        //-----------------------------------------
        string msg = dataJson.toJsonString();
        byte[] msgBytes = msg.toBytes();

        //-----------------------------------------
        //         SEND MESSAGE
        //-----------------------------------------
        var sendResult = producer->send(msgBytes, topic);
        if sendResult is error {
            io:println("failed to send message: ", sendResult);
        } else {
            //--------------------------------
            //    AWAIT RESPONSE
            //--------------------------------
            io:println("\n...waiting for response from servers...\n");

            responseList.removeAll();

            int waitCount = 0;
            while responseList.length() < numOfResponses {
                io:print("");                //  SOMETHING NEEDS TO BE DONE IN THE LOOP OR ELSE IT WILL BLOCK
                runtime:sleep(500);                //  CAN BE USED IN FUTURE UPDATE TO HANDLE WAITING FOR RESPONSES
                waitCount += 1;
                if waitCount == 8 {
                    return;
                }
            }
        }

    } else {
        io:println(dataJson.detail());
    }
}
//-----------------------------------------------------------
//
//
//
//
//-----------------------------------------------------------
//         ADD USER TEST  -->  ADDS 3 DIFFERENT USERS
//-----------------------------------------------------------
function addUser() {

    Request req = {
        reqType: TYPE_ADD_USER_REQ,
        data: ""
    };

    //---------------------
    //    ADD HOD
    //---------------------
    io:println("--------------------");
    io:println("    ADDING HOD");
    io:println("--------------------");
    AddUserRequest addUserReq = {
        clientTopic: clientID,
        userType: USER_HOD,
        username: "rg",
        password: "pw"
    };

    json|error jsonData = toJson(addUserReq);
    if jsonData is json {
        req.data = jsonData.toJsonString();
        produceMessage("user_service", req, 1);
        if responseList.length() > 0 {
            io:println(responseList[0]);
        }
    }

    //---------------------
    //    ADD LECTURER
    //---------------------
    io:println("--------------------");
    io:println("  ADDING LECTURER");
    io:println("--------------------");

    addUserReq.userType = USER_LECTURER;
    addUserReq.username = "ab";

    jsonData = toJson(addUserReq);
    if jsonData is json {
        req.data = jsonData.toJsonString();
        produceMessage("user_service", req, 1);
        if responseList.length() > 0 {
            io:println(responseList[0]);
        }
    }

    //---------------------
    //    ADD STUDENT
    //---------------------
    io:println("--------------------");
    io:println("  ADDING STUDENT");
    io:println("--------------------");

    addUserReq.userType = USER_STUDENT;
    addUserReq.username = "cd";

    jsonData = toJson(addUserReq);
    if jsonData is json {
        req.data = jsonData.toJsonString();
        produceMessage("user_service", req, 1);
        if responseList.length() > 0 {
            io:println(responseList[0]);
        }
    }
}
//-----------------------------------------------------------
//
//
//
//
//-----------------------------------------------------------
//         LOGIN USER TEST  -->  LOGS IN 3 DIFFERENT USERS
//-----------------------------------------------------------
function loginUser() {

    Request req = {
        reqType: TYPE_LOGIN_USER_REQ,
        data: ""
    };

    //---------------------
    //    LOGIN HOD
    //---------------------
    io:println("--------------------");
    io:println("    LOGGING IN HOD");
    io:println("--------------------");
    AddUserRequest loginUserReq = {
        clientTopic: clientID,
        userType: USER_HOD,
        username: "rg",
        password: "pw"
    };

    json|error jsonData = toJson(loginUserReq);
    if jsonData is json {
        req.data = jsonData.toJsonString();
        produceMessage("user_service", req, 1);
        if responseList.length() > 0 {
            io:println(responseList[0]);
        }
    }

    //---------------------
    //    LOGIN LECTURER
    //---------------------
    io:println("-----------------------");
    io:println("  LOGGING IN LECTURER");
    io:println("-----------------------");

    loginUserReq.userType = USER_LECTURER;
    loginUserReq.username = "ab";

    jsonData = toJson(loginUserReq);
    if jsonData is json {
        req.data = jsonData.toJsonString();
        produceMessage("user_service", req, 1);
        if responseList.length() > 0 {
            io:println(responseList[0]);
        }
    }

    //---------------------
    //    LOGIN STUDENT
    //---------------------
    io:println("----------------------");
    io:println("  LOGGING IN STUDENT");
    io:println("----------------------");

    loginUserReq.userType = USER_STUDENT;
    loginUserReq.username = "cd";

    jsonData = toJson(loginUserReq);
    if jsonData is json {
        req.data = jsonData.toJsonString();
        produceMessage("user_service", req, 1);
        if responseList.length() > 0 {
            io:println(responseList[0]);
        }
    }
}
//-----------------------------------------------------------
//
//
//
//
//-----------------------------------------------------------
//         ADD COURSE TEST  -->  ADDS 2 DIFFERENT COURSES
//-----------------------------------------------------------
function addCourse() {
    Request req = {
        reqType: TYPE_ADD_COURSE_REQ,
        data: ""
    };


    //---------------------
    //    ADD ICE COURSE
    //---------------------
    io:println("--------------------");
    io:println("  ADDING ICE COURSE");
    io:println("--------------------");

    AddCourseRequest addCourseReq = {
        clientTopic: clientID,
        hodUsername: "rg",        // HOD USERNAME
        courseName: "ICE",
        lectUsername: "ab",
        desc: "description of course"
    };

    json|error jsonData = toJson(addCourseReq);
    io:println(addCourseReq);
    if jsonData is json {
        req.data = jsonData.toJsonString();
        produceMessage("course_service", req, 1);
        if responseList.length() > 0 {
            io:println(responseList[0]);
        }
    }

    //---------------------
    //    ADD DSA COURSE
    //---------------------
    io:println("--------------------");
    io:println("  ADDING DSA COURSE");
    io:println("--------------------");

    addCourseReq.courseName = "DSA";

    jsonData = toJson(addCourseReq);
    io:println(addCourseReq);
    if jsonData is json {
        req.data = jsonData.toJsonString();
        produceMessage("course_service", req, 1);
        if responseList.length() > 0 {
            io:println(responseList[0]);
        }
    }
}
//-----------------------------------------------------------
//
//
//
//
//---------------------------------------------------------------
//         UPDATE COURSE TEST  -->  UPDATES 2 DIFFERENT COURSES
//---------------------------------------------------------------
function updateCourse() {
    Request req = {
        reqType: TYPE_UPDATE_COURSE_REQ,
        data: ""
    };


    //-------------------------------------
    //    UPDATE ICE COURSE AS HOD
    //-------------------------------------
    io:println("------------------------");
    io:println("  UPDATING ICE COURSE");
    io:println("------------------------");

    UpdateCourseRequest updateCourseReq = {
        clientTopic: clientID,
        username: "rg",
        courseName: "ICE",
        desc: "updated description of course"
    };

    json|error jsonData = toJson(updateCourseReq);
    if jsonData is json {
        req.data = jsonData.toJsonString();
        produceMessage("course_service", req, 1);
        if responseList.length() > 0 {
            io:println(responseList[0]);
        }
    }

    //-------------------------------------
    //    UPDATE DSA COURSE AS LECTURER
    //-------------------------------------
    io:println("------------------------");
    io:println("  UPDATING DSA COURSE");
    io:println("------------------------");

    updateCourseReq.username = "ab";
    updateCourseReq.courseName = "DSA";
    updateCourseReq.desc = "another updated description";

    jsonData = toJson(updateCourseReq);
    if jsonData is json {
        req.data = jsonData.toJsonString();
        produceMessage("course_service", req, 1);
        if responseList.length() > 0 {
            io:println(responseList[0]);
        }
    }
}
//-----------------------------------------------------------
//
//
//
//
//------------------------------------------------------------------------------
//   ADD STUDENT COURSE TEST  -->  REGISTERS STUDENT FOR 2 DIFFERENT COURSES
//------------------------------------------------------------------------------
function addStudentCourse() {
    Request req = {
        reqType: TYPE_ADD_STUDENT_COURSE_REQ,
        data: ""
    };


    //---------------------
    //    REG ICE COURSE
    //---------------------
    io:println("--------------------");
    io:println("  REG ICE COURSE");
    io:println("--------------------");

    AddStudentCourseRequest addStudentCourseReq = {
        clientTopic: clientID,
        studentUsername: "cd",        // STUDENT UNAME
        courseName: "ICE"
    };

    json|error jsonData = toJson(addStudentCourseReq);
    io:println(addStudentCourseReq);
    if jsonData is json {
        req.data = jsonData.toJsonString();
        produceMessage("course_service", req, 1);
        if responseList.length() > 0 {
            io:println(responseList[0]);
        }
    }

    //---------------------
    //    REG DSA COURSE
    //---------------------
    io:println("--------------------");
    io:println("  ADDING DSA COURSE");
    io:println("--------------------");

    addStudentCourseReq.courseName = "DSA";

    jsonData = toJson(addStudentCourseReq);
    io:println(addStudentCourseReq);
    if jsonData is json {
        req.data = jsonData.toJsonString();
        produceMessage("course_service", req, 1);
        if responseList.length() > 0 {
            io:println(responseList[0]);
        }
    }
}
//-----------------------------------------------------------
//
//
//
//
//------------------------------------------------------------------------------
//   GET STUDENT COURSES TEST  -->  GET REGISTERED COURSE FOR cd STUDENT
//------------------------------------------------------------------------------
function getStudentCourses() {
    Request req = {
        reqType: TYPE_GET_STUDENT_COURSES_REQ,
        data: ""
    };


    //--------------------------
    //    GET COURSES FOR cd
    //--------------------------
    io:println("--------------------------");
    io:println("  GETTING COURSES FOR cd");
    io:println("--------------------------");

    GetStudentCoursesRequest getStudentCoursesReq = {
        clientTopic: clientID,
        studentUsername: "cd"    // STUDENT UNAME
    };

    json|error jsonData = toJson(getStudentCoursesReq);
    if jsonData is json {
        req.data = jsonData.toJsonString();
        produceMessage("course_service", req, 1);
        if responseList.length() > 0 {
            io:println(responseList[0]);
        }
    }
}
//-----------------------------------------------------------
//
//
//
//
//-----------------------------------------------------------
//   ADD OUTLINE TEST  -->  ADDS AN OUTLINE FOR EACH COURSE
//-----------------------------------------------------------
function addOutline() {
    Request req = {
        reqType: TYPE_ADD_OUTLINE_REQ,
        data: ""
    };


    //---------------------
    //    ADD ICE OUTLINE
    //---------------------
    io:println("----------------------");
    io:println("  ADDING ICE OUTLINE");
    io:println("----------------------");

    AddOutlineRequest addOutlineReq = {
        clientTopic: clientID,
        courseName: "ICE",
        lecturerUsername: "ab",
        signature: "",
        learningOutcomes: "1. learn the fundementals about starting a business \n 2. learn how to draw up a business plan",
        content: "* entrepreneuership fundementals \n* registering a business \n* setting up the initial business",
        courseSchedule: "week1: introduction \n week2: case study on businesses in south africa",
        assessmentSchedule: "week4: assignment1 due \nweek8: group assignment due"
    };

    //--------------------------------
    //    GENERATE SIG FOR OUTLINE
    //--------------------------------
    string dataToSign = addOutlineReq.courseName + addOutlineReq.lecturerUsername;
    string|error sig = generateSignature(dataToSign);

    if sig is error {
        io:println(sig.detail());
    } else {

        //-------------------------------
        //    ADD SIG TO DATA
        //-------------------------------
        addOutlineReq.signature = sig;

        json|error jsonData = toJson(addOutlineReq);
        if jsonData is json {
            req.data = jsonData.toJsonString();
            produceMessage("outline_service", req, 1);
            if responseList.length() > 0 {
                io:println(responseList[0]);
            }
        } else {
            io:println("failed to convert to json");
        }
    }

    //---------------------
    //    ADD DSA OUTLINE
    //---------------------
    io:println("----------------------");
    io:println("  ADDING DSA OUTLINE");
    io:println("----------------------");

    addOutlineReq.courseName = "DSA";
    addOutlineReq.learningOutcomes = "1.  Analyze the principles and mechanisms used in Distributed Systems \n 2. Demonstrate knowledge of the current technologies and their application \n 3. Design and develop distributed applications";
    addOutlineReq.content = "• Distributed Systems and their characteristics \n • Applications: Service‐oriented Computing \n • Distributed Algorithms";
    addOutlineReq.courseSchedule = "week1: introduction \n week2: Distributed Systems and their Characteristics \n week3: Remote Invocation";
    addOutlineReq.assessmentSchedule = "06/09/2021: assignment1 \n 30/09/2021: assignment2 \n 21/10/2021: assignment3";

    //--------------------------------
    //    GENERATE SIG FOR OUTLINE
    //--------------------------------
    dataToSign = addOutlineReq.courseName + addOutlineReq.lecturerUsername;
    sig = generateSignature(dataToSign);

    if sig is error {
        io:println(sig.detail());
    } else {

        //-------------------------------
        //    ADD SIG TO DATA
        //-------------------------------
        addOutlineReq.signature = sig;

        json|error jsonData = toJson(addOutlineReq);
        if jsonData is json {
            req.data = jsonData.toJsonString();
            produceMessage("outline_service", req, 1);
            if responseList.length() > 0 {
                io:println(responseList[0]);
            }
        } else {
            io:println("failed to convert to json");
        }
    }
}
//-----------------------------------------------------------
//
//
//
//
//-----------------------------------------------------------------------
//   GET OUTLINE TEST  -->  GETS OUTLINE FOR EACH COURSE FOR STUDENT
//-----------------------------------------------------------------------
function getOutline() {
    Request req = {
        reqType: TYPE_GET_OUTLINE_REQ,
        data: ""
    };


    //--------------------------------
    //    GET ICE OUTLINE FOR HOD
    //--------------------------------
    io:println("-------------------------------");
    io:println("  GETTING ICE OUTLINE FOR HOD");
    io:println("-------------------------------");

    GetOutlineRequest getOutlineReq = {
        clientTopic: clientID,
        userType: USER_HOD,
        courseName: "ICE",
        username: "rg"
    };

    json|error jsonData = toJson(getOutlineReq);
    io:println(getOutlineReq);
    if jsonData is json {
        req.data = jsonData.toJsonString();
        produceMessage("outline_service", req, 1);
        if responseList.length() > 0 {
            io:println(responseList[0]);
        }
    }

    //------------------------------------
    //    GET ICE OUTLINE FOR STUDENT
    //------------------------------------
    io:println("------------------------------------");
    io:println("  GETTING ICE OUTLINE FOR STUDENT");
    io:println("------------------------------------");

    getOutlineReq.userType = USER_STUDENT;
    getOutlineReq.username = "cd";

    jsonData = toJson(getOutlineReq);
    io:println(getOutlineReq);
    if jsonData is json {
        req.data = jsonData.toJsonString();
        produceMessage("outline_service", req, 1);
        if responseList.length() > 0 {
            io:println(responseList[0]);
        }
    }

    //------------------------------------
    //    GET ICE OUTLINE FOR LECTURER
    //------------------------------------
    io:println("------------------------------------");
    io:println("  GETTING ICE OUTLINE FOR LECTURER");
    io:println("------------------------------------");

    getOutlineReq.userType = USER_LECTURER;
    getOutlineReq.username = "ab";

    jsonData = toJson(getOutlineReq);
    io:println(getOutlineReq);
    if jsonData is json {
        req.data = jsonData.toJsonString();
        produceMessage("outline_service", req, 1);
        if responseList.length() > 0 {
            io:println(responseList[0]);
        }
    }
}
//-----------------------------------------------------------
//
//
//
//
//-----------------------------------------------------------
//   GET ALL OUTLINES TEST  -->  GETS ALL OUTLINES AS HOD
//-----------------------------------------------------------
function getAllOutlines() {
    Request req = {
        reqType: TYPE_GET_ALL_OUTLINES_REQ,
        data: ""
    };


    //--------------------------------
    //    GET ALL OUTLINES AS HOD
    //--------------------------------
    io:println("-------------------------------");
    io:println("  GET ALL OUTLINES AS HOD");
    io:println("-------------------------------");

    GetOutlinesRequest getOutlinesReq = {
        clientTopic: clientID,
        username: "rg"
    };

    json|error jsonData = toJson(getOutlinesReq);
    if jsonData is json {
        req.data = jsonData.toJsonString();
        produceMessage("outline_service", req, 1);
        if responseList.length() > 0 {
            io:println(responseList[0]);
        }
    }
}
//---------------------------------------------------------------
//
//
//
//
//-------------------------------------------------------------------------
//   GET OUTLINES BY LECTURER TEST  -->  GETS ALL OUTLINES BY A LECTURER
//-------------------------------------------------------------------------
function getOutlinesByLecturer() {
    Request req = {
        reqType: TYPE_GET_OUTLINES_BY_LECTURER_REQ,
        data: ""
    };


    //--------------------------------
    //    GET OUTLINES BY LECTURER
    //--------------------------------
    io:println("-------------------------------");
    io:println("  GET OUTLINES BY LECTURER");
    io:println("-------------------------------");

    GetOutlinesRequest getOutlinesReq = {
        clientTopic: clientID,
        username: "ab"
    };

    json|error jsonData = toJson(getOutlinesReq);
    if jsonData is json {
        req.data = jsonData.toJsonString();
        produceMessage("outline_service", req, 1);
        if responseList.length() > 0 {
            io:println(responseList[0]);
        }
    }
}
//---------------------------------------------------------------
//
//
//
//
//---------------------------------------------------------------
//   APPROVE OUTLINE TEST  -->  APPROVE OUTLINE FOR EACH COURSE
//---------------------------------------------------------------
function approveOutline() {
    Request req = {
        reqType: TYPE_APPROVE_OUTLINE_REQ,
        data: ""
    };

    //--------------------------
    //    APPROVE ICE OUTLINE
    //--------------------------
    io:println("----------------------------");
    io:println("  APPROVING ICE OUTLINE ");
    io:println("----------------------------");

    ApproveOutlineRequest apprOutlineReq = {
        clientTopic: clientID,
        hodUsername: "rg",
        signature: "",
        courseName: "ICE"
    };

    //--------------------------------
    //    GENERATE HOD SIGNATURE
    //--------------------------------
    string dataToSign = apprOutlineReq.courseName + apprOutlineReq.hodUsername;
    string|error sig = generateSignature(dataToSign);

    if sig is error {
        io:println(sig.detail());
    } else {

        //-------------------------------
        //    ADD SIG TO DATA
        //-------------------------------
        apprOutlineReq.signature = sig;

        json|error jsonData = toJson(apprOutlineReq);


        if jsonData is json {
            req.data = jsonData.toJsonString();
            produceMessage("outline_service", req, 1);
            if responseList.length() > 0 {
                io:println(responseList[0]);
            }
        }
    }

    //--------------------------
    //    APPROVE DSA OUTLINE
    //--------------------------
    io:println("----------------------------");
    io:println("  APPROVING DSA OUTLINE ");
    io:println("----------------------------");

    apprOutlineReq.courseName = "DSA";

    //--------------------------------
    //    GENERATE HOD SIGNATURE
    //--------------------------------
    dataToSign = apprOutlineReq.courseName + apprOutlineReq.hodUsername;
    sig = generateSignature(dataToSign);

    if sig is error {
        io:println(sig.detail());
    } else {

        //-------------------------------
        //    ADD SIG TO DATA
        //-------------------------------
        apprOutlineReq.signature = sig;

        json|error jsonData = toJson(apprOutlineReq);

        if jsonData is json {
            req.data = jsonData.toJsonString();
            produceMessage("outline_service", req, 1);
            if responseList.length() > 0 {
                io:println(responseList[0]);
            }
        }
    }
}
//-----------------------------------------------------------
//
//
//
//
//---------------------------------------------------------------
//   ACK OUTLINE TEST  -->  ACK OUTLINE FOR EACH COURSE
//---------------------------------------------------------------
function ackOutline() {
    Request req = {
        reqType: TYPE_ACK_OUTLINE_REQ,
        data: ""
    };


    //--------------------------
    //    ACK ICE OUTLINE
    //--------------------------
    io:println("----------------------------");
    io:println("  ACKNOWLEDGING ICE OUTLINE ");
    io:println("----------------------------");

    AckOutlineRequest ackOutlineReq = {
        clientTopic: clientID,
        studentUsername: "cd",
        courseName: "ICE"
    };

    json|error jsonData = toJson(ackOutlineReq);
    io:println(ackOutlineReq);
    if jsonData is json {
        req.data = jsonData.toJsonString();
        produceMessage("outline_service", req, 1);
        if responseList.length() > 0 {
            io:println(responseList[0]);
        }
    }

    //--------------------------
    //    ACK DSA OUTLINE
    //--------------------------
    io:println("----------------------------");
    io:println("  ACKNOWLEDGING DSA OUTLINE ");
    io:println("----------------------------");

    ackOutlineReq.courseName = "DSA";

    jsonData = toJson(ackOutlineReq);
    io:println(ackOutlineReq);
    if jsonData is json {
        req.data = jsonData.toJsonString();
        produceMessage("outline_service", req, 1);
        if responseList.length() > 0 {
            io:println(responseList[0]);
        }
    }
}
//-----------------------------------------------------------
//
//
//
//
//-----------------------------------------------------------
//                GENERATE SIGNATURE
//-----------------------------------------------------------
function generateSignature(string data) returns string|error {
    io:println("generating signature");

    //-------------------------------
    //       SET KEYSTORE DETAILS
    //-------------------------------
    crypto:KeyStore ks = {
        path: "./keys/keystore.p12",
        password: "admin"
    };

    //--------------------------------------
    //     READ PRIVATE KEY FROM KEYSTORE
    //--------------------------------------
    var privateKey = crypto:decodePrivateKey(ks, "1", "admin");
    io:println("reached: ", privateKey);

    //-------------------------------
    //     CHECK FOR ERROR
    //-------------------------------
    if privateKey is crypto:PrivateKey {

        io:println("signing data");

        //-------------------------------
        //         SIGN DATA
        //-------------------------------
        byte[]|error signature = crypto:signRsaSha256(data.toBytes(), privateKey);

        if signature is byte[] {

            io:println("successfully signed data");

            //-------------------------------
            //   RETURN SIG IN HEX FORMAT
            //-------------------------------
            return signature.toBase16();

        } else {

            io:println("failed to sign data");
            return signature;

        }
    } else {
        io:println("error reading key data: ", privateKey.detail());
        return privateKey;
    }
}
