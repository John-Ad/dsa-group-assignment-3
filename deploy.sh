#################################################
# DEPLOY USER SERVICE
#################################################

echo "###########################"
echo "DEPLOYING USER SERVICE"
echo "###########################"

cd user_service
bal build --skip-tests main
cp ./target/bin/main.jar ./docker/
cd docker
eval $(minikube -p minikube docker-env)
docker build . -t user-service:latest
cd ..
helm uninstall user-service-deployment
helm install user-service-deployment ./user-service-deployment
#################################################

cd ..

#################################################
# DEPLOY COURSE SERVICE
#################################################

echo "############################"
echo "DEPLOYING COURSE SERVICE"
echo "############################"

cd course_service
bal build --skip-tests main
cp ./target/bin/main.jar ./docker/
cd docker
eval $(minikube -p minikube docker-env)
docker build . -t course-service:latest
cd ..
helm uninstall course-service-deployment
helm install course-service-deployment ./course-service-deployment
#################################################

cd ..

#################################################
# DEPLOY OUTLINE SERVICE
#################################################

echo "############################"
echo "DEPLOYING OUTLINE SERVICE"
echo "############################"

cd outline_service
bal build --skip-tests main
cp ./target/bin/main.jar ./docker/
cd docker
eval $(minikube -p minikube docker-env)
docker build . -t outline-service:latest
cd ..
helm uninstall outline-service-deployment
helm install outline-service-deployment ./outline-service-deployment
#################################################
